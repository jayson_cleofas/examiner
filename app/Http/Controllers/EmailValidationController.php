<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class EmailValidationController extends Controller
{
    public function store(Request $request)
    {
        $request->validate([
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
        ]);

        return response()->json(['status' => 'ok']);
    }
}
